package router

import (
	"citizenv/controller"
	_ "citizenv/docs"
	"citizenv/infrastructure"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
	httpSwagger "github.com/swaggo/http-swagger"
)

var (
	InfoLog = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrLog  = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
)

func Router() http.Handler {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.URLFormat)
	r.Use(middleware.RequestID)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	acceptCors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // Use this to allow specific origin hosts
		// AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  checkOrigin,
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(acceptCors.Handler)
	// Declare controller
	userController := controller.NewUserController()
	wardController := controller.NewWardController()
	provinceController := controller.NewProvinceController()
	districtController := controller.NewDistrictController()
	villageController := controller.NewVillageController()
	citizenController := controller.NewCitizenController()
	// swagger route
	r.Get("/api/v1/swagger/*", httpSwagger.Handler(
		httpSwagger.URL(infrastructure.GetHTTPSwagger()),
	))

	r.Route("/api/v1", func(router chi.Router) {
		// public routes
		router.Post("/user/login", userController.Login)
		router.Post("/user/login/jwt", userController.LoginWithToken)

		// protected routes
		router.Group(func(protectedRoute chi.Router) {
			// Declare middleware
			protectedRoute.Use(jwtauth.Verifier(infrastructure.GetEncodeAuth()))
			protectedRoute.Use(jwtauth.Authenticator)

			//---------------User routes--------------------------------
			protectedRoute.Route("/user", func(subRoute chi.Router) {
				subRoute.Post("/create", userController.CreateUser)
				subRoute.Get("/all", userController.GetAll)
				subRoute.Delete("/delete/{uid}", userController.DeleteUser)
				subRoute.Get("/wname", userController.GetByUsername)
				subRoute.Put("/setPerm", userController.SetPermission)
				subRoute.Get("/getchild", userController.GetChildUser)
				subRoute.Get("/getcitizen", userController.GetChildCitizen)
				subRoute.Get("/progress", userController.GetCensusProgress)
				subRoute.Put("/set_progress", userController.SetProgress)
				subRoute.Get("/sex_chart", userController.GetSexChart)
				subRoute.Get("/age_chart", userController.GetAgeChart)
			})
			//---------------Province routes----------------------------
			protectedRoute.Route("/province", func(subRoute chi.Router) {
				subRoute.Get("/all", provinceController.GetAll)
				subRoute.Get("/{id}", provinceController.GetById)
				subRoute.Get("/wcode", provinceController.GetByProvinceCode)
				subRoute.Get("/wn", provinceController.GetByProvinceName)
				subRoute.Post("/create", provinceController.CreateProvince)
				subRoute.Delete("/delete_id/{id}", provinceController.DeleteByProvinceId)
				subRoute.Delete("/delete_code", provinceController.DeleteByProvinceCode)
			})
			//---------------District routes----------------------------
			protectedRoute.Route("/district", func(subRoute chi.Router) {
				subRoute.Get("/all", districtController.GetAll)
				subRoute.Get("/{id}", districtController.GetById)
				subRoute.Get("/wcode", districtController.GetByDistrictCode)
				subRoute.Get("/wn", districtController.GetByDistrictName)
				subRoute.Post("/create", districtController.CreateDistrict)
				subRoute.Delete("/delete_id/{id}", districtController.DeleteByDistrictId)
				subRoute.Delete("/delete_code", districtController.DeleteByDistrictCode)
			})
			//---------------Ward routes--------------------------------
			protectedRoute.Route("/ward", func(subRoute chi.Router) {
				subRoute.Get("/all", wardController.GetAll)
				subRoute.Get("/{id}", wardController.GetById)
				subRoute.Get("/wcode", wardController.GetByWardCode)
				subRoute.Get("/wn", wardController.GetByWardName)
				subRoute.Post("/create", wardController.CreateWard)
				subRoute.Delete("/delete_id/{id}", wardController.DeleteByWardId)
				subRoute.Delete("/delete_code", wardController.DeleteByWardCode)
			})
			//---------------Village routes--------------------------------
			protectedRoute.Route("/village", func(subRoute chi.Router) {
				subRoute.Get("/all", villageController.GetAll)
				subRoute.Get("/{id}", villageController.GetById)
				subRoute.Get("/wcode", villageController.GetByVillageCode)
				subRoute.Get("/wn", villageController.GetByVillageName)
				subRoute.Post("/create", villageController.CreateVillage)
				subRoute.Delete("/delete_id/{id}", villageController.DeleteByVillageId)
				subRoute.Delete("/delete_code", villageController.DeleteByVillageCode)
			})
			//---------------Citizen routes----------------------------
			protectedRoute.Route("/citizen", func(subRoute chi.Router) {
				subRoute.Get("/all", citizenController.GetAll)
				subRoute.Get("/id-get/{id}", citizenController.GetById)
				subRoute.Get("/ident-get/{ident}", citizenController.GetByIdentityNumber)
				subRoute.Post("/create", citizenController.CreateCitizen)
				subRoute.Delete("/delete/{id}", citizenController.DeleteCitizen)
				subRoute.Put("/update", citizenController.UpdateCitizen)
			})

		})
	})

	return r
}
