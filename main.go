package main

import (
	"citizenv/infrastructure"
	"citizenv/router"
	"log"
	"net/http"
)

// @title PULIC
// @version 1.0
// @description PULIC Project APIs list

// @host localhost:10050
// @BasePath /api/v1

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	log.Println("Database name: ", infrastructure.GetDBName())
	log.Fatal(http.ListenAndServe(":"+infrastructure.GetAppPort(), router.Router()))

}
