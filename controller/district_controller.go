package controller

import (
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type districtController struct {
	districtService service.DistrictService
}

type DistrictController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByDistrictCode(w http.ResponseWriter, r *http.Request)
	GetByDistrictName(w http.ResponseWriter, r *http.Request)
	CreateDistrict(w http.ResponseWriter, r *http.Request)
	DeleteByDistrictId(w http.ResponseWriter, r *http.Request)
	DeleteByDistrictCode(w http.ResponseWriter, r *http.Request)
}

// GetAll gets all districts in the database
// @tags districts-api-manager
// @summary get all districts in the database
// @description: no input => []district
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /district/all [get]
func (c *districtController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	districts, err := c.districtService.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.WriteHeader(http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    districts,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetById gets all districts in the database with given id
// @tags districts-api-manager
// @summary get all districts in the database
// @description: input district_id => district
// @Accept json
// @Produce json
// @Param id path integer true "district's id"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /district/{id} [get]
func (c *districtController) GetById(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	district, err := c.districtService.GetById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    district,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// GetByDistrictCode gets all districts in the database with given code
// @tags districts-api-manager
// @summary get all districts in the database with given code
// @description: input district_code => district
// @Accept json
// @Produce json
// @Param code query string true "district's code"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /district/wcode [get]
func (c *districtController) GetByDistrictCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	district, err := c.districtService.GetByDistrictCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    district,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByDistrictName gets all districts in the database with given name
// @tags districts-api-manager
// @summary get all districts in the database with given name
// @description: input district_name => district
// @Accept json
// @Produce json
// @Param name query string true "district name"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /district/wn [get]
func (c *districtController) GetByDistrictName(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	name := r.URL.Query().Get("name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "district name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	district, err := c.districtService.GetByDistrictName(name)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    district,
			Message: "Successfully retrieved",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)

}

// CreateDistrict creates a new district with given data
// @tags districts-api-manager
// @Summary Create a new district with given data
// @Description district data => new district in db
// @Accept json
// @Produce json
// @Param districtInfo body model.District true "district information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /district/create [post]
func (c *districtController) CreateDistrict(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var newDistrict model.District
	token := r.Header.Get("Authorization")
	user, _ := middleware.GetClaimsData(token)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newDistrict); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	newDistrict.ProvinceCode = user.Username

	district, err := c.districtService.CreateDistrict(&newDistrict)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    district,
			Message: "Successfully created",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// DeleteByDistrictId deletes district(s) with given id
// @tags districts-api-manager
// @Summary Delete district(s) with given id
// @Description input district_id => delete district with that district_id in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param id path integer true "to be deleted district's id"
// @Success 200 {object} model.Response
// @Router /district/delete_id/{id} [delete]
func (c *districtController) DeleteByDistrictId(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err = c.districtService.DeleteByDistrictId(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// DeleteByDistrictCode deletes district(s) with given code
// @tags districts-api-manager
// @Summary Delete district(s) with given code
// @Description input district code => delete district with that district code in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param code query string true "to be deleted district's id"
// @Success 200 {object} model.Response
// @Router /district/delete_code [delete]
func (c *districtController) DeleteByDistrictCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err := c.districtService.DeleteByDistrictCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

func NewDistrictController() DistrictController {
	return &districtController{
		districtService: service.NewDistrictService(),
	}
}
