package controller

import (
	"citizenv/model"
	"citizenv/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type provinceController struct {
	provinceService service.ProvinceService
}

type ProvinceController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByProvinceCode(w http.ResponseWriter, r *http.Request)
	GetByProvinceName(w http.ResponseWriter, r *http.Request)
	CreateProvince(w http.ResponseWriter, r *http.Request)
	DeleteByProvinceId(w http.ResponseWriter, r *http.Request)
	DeleteByProvinceCode(w http.ResponseWriter, r *http.Request)
}

// GetAll gets all provinces in the database
// @tags provinces-api-manager
// @summary get all provinces in the database
// @description: no input => []province
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /province/all [get]
func (c *provinceController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	provinces, err := c.provinceService.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.WriteHeader(http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    provinces,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetById gets all provinces in the database with given id
// @tags provinces-api-manager
// @summary get all provinces in the database
// @description: input province_id => province
// @Accept json
// @Produce json
// @Param id path integer true "province's id"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /province/{id} [get]
func (c *provinceController) GetById(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	province, err := c.provinceService.GetById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    province,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// GetByProvinceCode gets all provinces in the database with given code
// @tags provinces-api-manager
// @summary get all provinces in the database with given code
// @description: input province_code => province
// @Accept json
// @Produce json
// @Param code query string true "province's code"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /province/wcode [get]
func (c *provinceController) GetByProvinceCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	province, err := c.provinceService.GetByProvinceCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    province,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByProvinceName gets all provinces in the database with given name
// @tags provinces-api-manager
// @summary get all provinces in the database with given name
// @description: input province_name => province
// @Accept json
// @Produce json
// @Param name query string true "province name"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /province/wn [get]
func (c *provinceController) GetByProvinceName(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	name := r.URL.Query().Get("name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "province name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	province, err := c.provinceService.GetByProvinceName(name)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    province,
			Message: "Successfully retrieved",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)

}

// CreateProvince creates a new province with given data
// @tags provinces-api-manager
// @Summary Create a new province with given data
// @Description province data => new province in db
// @Accept json
// @Produce json
// @Param provinceInfo body model.Province true "province information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /province/create [post]
func (c *provinceController) CreateProvince(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var newProvince model.Province
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newProvince); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	province, err := c.provinceService.CreateProvince(&newProvince)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    province,
			Message: "Successfully created",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// DeleteByProvinceId deletes province(s) with given id
// @tags provinces-api-manager
// @Summary Delete province(s) with given id
// @Description input province_id => delete province with that province_id in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param id path integer true "to be deleted wprovince's id"
// @Success 200 {object} model.Response
// @Router /province/delete_id/{id} [delete]
func (c *provinceController) DeleteByProvinceId(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err = c.provinceService.DeleteByProvinceId(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// DeleteByProvinceCode deletes province(s) with given code
// @tags provinces-api-manager
// @Summary Delete province(s) with given code
// @Description input province code => delete province with that province code in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param code query string true "to be deleted province's id"
// @Success 200 {object} model.Response
// @Router /province/delete_code [delete]
func (c *provinceController) DeleteByProvinceCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err := c.provinceService.DeleteByProvinceCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

func NewProvinceController() ProvinceController {
	return &provinceController{
		provinceService: service.NewProvinceService(),
	}
}
