package controller

import (
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type villageController struct {
	villageService service.VillageService
}

type VillageController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByVillageCode(w http.ResponseWriter, r *http.Request)
	GetByVillageName(w http.ResponseWriter, r *http.Request)
	CreateVillage(w http.ResponseWriter, r *http.Request)
	DeleteByVillageId(w http.ResponseWriter, r *http.Request)
	DeleteByVillageCode(w http.ResponseWriter, r *http.Request)
}

// GetAll gets all Villages in the database
// @tags Villages-api-manager
// @summary get all Villages in the database
// @description: no input => []Village
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /village/all [get]
func (c *villageController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	villages, err := c.villageService.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.WriteHeader(http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    villages,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetById gets all Villages in the database with given id
// @tags Villages-api-manager
// @summary get all Villages in the database
// @description: input Village_id => Village
// @Accept json
// @Produce json
// @Param id path integer true "Village's id"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /village/{id} [get]
func (c *villageController) GetById(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	village, err := c.villageService.GetById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    village,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// GetByVillageCode gets all Villages in the database with given code
// @tags Villages-api-manager
// @summary get all Villages in the database with given code
// @description: input Village_code => Village
// @Accept json
// @Produce json
// @Param code query string true "Village's code"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /village/wcode [get]
func (c *villageController) GetByVillageCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "village code is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	village, err := c.villageService.GetByVillageCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    village,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByVillageName gets all Villages in the database with given name
// @tags Villages-api-manager
// @summary get all Villages in the database with given name
// @description: input Village_name => Village
// @Accept json
// @Produce json
// @Param name query string true "Village name"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /village/wn [get]
func (c *villageController) GetByVillageName(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	name := r.URL.Query().Get("name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "village name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	village, err := c.villageService.GetByVillageName(name)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    village,
			Message: "Successfully retrieved",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)

}

// CreateVillage creates a new Village with given data
// @tags Villages-api-manager
// @Summary Create a new Village with given data
// @Description Village data => new Village in db
// @Accept json
// @Produce json
// @Param VillageInfo body model.Village true "Village information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /village/create [post]
func (c *villageController) CreateVillage(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var newVillage model.Village
	token := r.Header.Get("Authorization")
	user, _ := middleware.GetClaimsData(token)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newVillage); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	newVillage.WardCode = user.Username
	village, err := c.villageService.CreateVillage(&newVillage)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    village,
			Message: "Successfully created",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// DeleteByVillageId deletes Village(s) with given id
// @tags Villages-api-manager
// @Summary Delete Village(s) with given id
// @Description input Village_id => delete Village with that Village_id in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param id path integer true "to be deleted Village's id"
// @Success 200 {object} model.Response
// @Router /village/delete_id/{id} [delete]
func (c *villageController) DeleteByVillageId(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err = c.villageService.DeleteByVillageId(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// DeleteByVillageCode deletes Village(s) with given code
// @tags Villages-api-manager
// @Summary Delete Village(s) with given code
// @Description input Village code => delete Village with that Village code in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param code query string true "to be deleted Village's id"
// @Success 200 {object} model.Response
// @Router /village/delete_code [delete]
func (c *villageController) DeleteByVillageCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "village name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err := c.villageService.DeleteByVillageCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

func NewVillageController() VillageController {
	return &villageController{
		villageService: service.NewVillageService(),
	}
}
