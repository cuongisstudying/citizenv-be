package controller

import (
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/service"
	"citizenv/utils"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type UserController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	CreateUser(w http.ResponseWriter, r *http.Request) // permission check
	DeleteUser(w http.ResponseWriter, r *http.Request) // permission check
	Login(w http.ResponseWriter, r *http.Request)
	LoginWithToken(w http.ResponseWriter, r *http.Request)
	GetByUsername(w http.ResponseWriter, r *http.Request)
	// CreateLocation(w http.ResponseWriter, r *http.Request)
	SetPermission(w http.ResponseWriter, r *http.Request) // permission check?
	GetChildUser(w http.ResponseWriter, r *http.Request)
	GetChildCitizen(w http.ResponseWriter, r *http.Request)
	GetCensusProgress(w http.ResponseWriter, r *http.Request)
	SetProgress(w http.ResponseWriter, r *http.Request)
	GetSexChart(w http.ResponseWriter, r *http.Request)
	GetAgeChart(w http.ResponseWriter, r *http.Request)
}

type userController struct {
	userService     service.UserService
	provinceService service.ProvinceService
	districtService service.DistrictService
	wardService     service.WardService
	villageService  service.VillageService
	citizenService  service.CitizenService
}

// GetAll gets all users currently in table "users"
// @tags user-manager-apis
// @Summary get all users
// @Description get all users
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/all [get]
func (c *userController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	users, err := c.userService.GetAll()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	} else {
		jsonResponse = &model.Response{
			Data:    users,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByUsername gets user currently in table "users" with username
// @tags user-manager-apis
// @Summary get user with usn
// @Description input username => user
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param username query string true "username"
// @Success 200 {object} model.Response
// @Router /user/wname [get]
func (c *userController) GetByUsername(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	username := r.URL.Query().Get("username")
	if username == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Message: "Username is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	user, err := c.userService.GetByUsername(username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    user,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// CreateUser creates an user with given data
// @tags user-manager-apis
// @Summary	creates new user
// @Description creates new user
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param UserInfo body model.User true "User information"
// @Success 200 {object} model.CreateResponse
// @Router /user/create [post]
func (c *userController) CreateUser(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.CreateResponse
	var newUser *model.User

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newUser); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	// Got new user's location name/username
	newUsernameInt, err := strconv.Atoi(newUser.Username)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	// Get user's role
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		jsonResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	// Got currently logged in user
	// current user is of role A1: can only
	// create two digits account, name in provinces table?
	if user.Role == "A1" {
		// check new user's username
		if newUsernameInt < 01 || newUsernameInt > 63 {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, http.StatusText(http.StatusBadRequest), 400)
			jsonResponse = &model.CreateResponse{
				Message: "A1 account can only create two digits account range 01 - 63",
				Success: false,
			}
			render.JSON(w, r, jsonResponse)
			return
		} else {
			// test location against db: cuz this is province, listed
			provinceInDb, err := c.provinceService.GetByProvinceCode(newUser.Username)
			if err != nil {
				jsonResponse = &model.CreateResponse{
					Message: err.Error(),
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if provinceInDb.Name == "" {
				jsonResponse = &model.CreateResponse{
					Message: "Province name not found in database, consider create one or check db again",
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			// got province in database
			// check if input LocationName == provinceInDb
			if provinceInDb.Name == newUser.LocationName {
				newUser.Role = "A2"
				newUserResponse, err := c.userService.CreateUser(newUser)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
					jsonResponse = &model.CreateResponse{
						Message: err.Error(),
						Success: false,
					}
				} else {
					jsonResponse = &model.CreateResponse{
						Id:           newUserResponse.Id,
						Username:     newUserResponse.Username,
						LocationName: newUserResponse.LocationName,
						Role:         newUserResponse.Role,
						Message:      "OK",
						Success:      true,
					}
				}
				render.JSON(w, r, jsonResponse)
			} else {
				jsonResponse = &model.CreateResponse{
					Message: "Invalid locationName and username:" + newUser.Username + " should be " + provinceInDb.Name + " but is " + newUser.LocationName,
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
		}
	} else if user.Role == "A2" {
		// check newUsername has prefix as user.Role
		if !strings.HasPrefix(newUser.Username, user.Username) || len(newUser.Username) != 4 {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, http.StatusText(http.StatusBadRequest), 400)
			jsonResponse = &model.CreateResponse{
				Message: "A2 user can only create 4-digits username starts with A2 username prefix!",
				Success: false,
			}
			render.JSON(w, r, jsonResponse)
			return
		} else {
			districtInDb, err := c.districtService.GetByDistrictCode(newUser.Username)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				http.Error(w, err.Error(), 500)
				jsonResponse = &model.CreateResponse{
					Message: err.Error(),
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			log.Println(districtInDb.Name)
			if districtInDb.Name == "" {
				jsonResponse = &model.CreateResponse{
					Message: "District name not found in database, consider create one or check db/location name again",
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if districtInDb.Name == newUser.LocationName {
				newUser.Role = "A3"
				newUserResponse, err := c.userService.CreateUser(newUser)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
					jsonResponse = &model.CreateResponse{
						Message: err.Error(),
						Success: false,
					}
				} else {
					jsonResponse = &model.CreateResponse{
						Id:           newUserResponse.Id,
						Username:     newUserResponse.Username,
						LocationName: newUserResponse.LocationName,
						Role:         newUserResponse.Role,
						Message:      "OK",
						Success:      true,
					}
				}
				render.JSON(w, r, jsonResponse)
				return
			} else {
				jsonResponse = &model.CreateResponse{
					Message: "Invalid locationName and username:" + newUser.Username + " should be " + districtInDb.Name + " but is " + newUser.LocationName,
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
		}
	} else if user.Role == "A3" {
		if !strings.HasPrefix(newUser.Username, user.Username) || len(newUser.Username) != 6 {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, http.StatusText(http.StatusBadRequest), 400)
			jsonResponse = &model.CreateResponse{
				Message: "A3 user can only create 6-digits username starts with A3 username prefix!",
				Success: false,
			}
			render.JSON(w, r, jsonResponse)
			return
		} else {
			wardInDb, err := c.wardService.GetByWardCode(newUser.Username)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				http.Error(w, err.Error(), 500)
				jsonResponse = &model.CreateResponse{
					Message: err.Error(),
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if wardInDb.Name == "" {
				jsonResponse = &model.CreateResponse{
					Message: "Ward name not found in database, consider create one or check db/location name again",
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if wardInDb.Name == newUser.LocationName {
				newUser.Role = "B1"
				newUserResponse, err := c.userService.CreateUser(newUser)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
					jsonResponse = &model.CreateResponse{
						Message: err.Error(),
						Success: false,
					}
				} else {
					jsonResponse = &model.CreateResponse{
						Id:           newUserResponse.Id,
						Username:     newUserResponse.Username,
						LocationName: newUserResponse.LocationName,
						Role:         newUserResponse.Role,
						Message:      "OK",
						Success:      true,
					}
				}
				render.JSON(w, r, jsonResponse)
				return
			} else {
				jsonResponse = &model.CreateResponse{
					Message: "Invalid locationName and username:" + newUser.Username + " should be " + wardInDb.Name + " but is " + newUser.LocationName,
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}

		}
	} else if user.Role == "B1" {
		if !strings.HasPrefix(newUser.Username, user.Username) || len(newUser.Username) != 8 {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, http.StatusText(http.StatusBadRequest), 400)
			jsonResponse = &model.CreateResponse{
				Message: "B1 user can only create 8-digits username starts with B1 username prefix!",
				Success: false,
			}
			render.JSON(w, r, jsonResponse)
			return
		} else {
			villageInDb, err := c.villageService.GetByVillageCode(newUser.Username)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				http.Error(w, err.Error(), 500)
				jsonResponse = &model.CreateResponse{
					Message: err.Error(),
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if villageInDb.Name == "" {
				jsonResponse = &model.CreateResponse{
					Message: "Village name not found in database, consider create one or check db/location name again",
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}
			if villageInDb.Name == newUser.LocationName {
				newUser.Role = "B2"
				newUserResponse, err := c.userService.CreateUser(newUser)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					http.Error(w, http.StatusText(http.StatusInternalServerError), 500)
					jsonResponse = &model.CreateResponse{
						Message: err.Error(),
						Success: false,
					}
				} else {
					jsonResponse = &model.CreateResponse{
						Id:           newUserResponse.Id,
						Username:     newUserResponse.Username,
						LocationName: newUserResponse.LocationName,
						Role:         newUserResponse.Role,
						Message:      "OK",
						Success:      true,
					}
				}
				render.JSON(w, r, jsonResponse)
			} else {
				jsonResponse = &model.CreateResponse{
					Message: "Invalid locationName and username:" + newUser.Username + " should be " + villageInDb.Name + " but is " + newUser.LocationName,
					Success: false,
				}
				render.JSON(w, r, jsonResponse)
				return
			}

		}
	}
}

// DeleteUser deletes user with UserID
// @tags user-manager-apis
// @Summary delete user
// @Description delete user
// @Accept json
// @Produce json
// @Param uid path integer true "User ID"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/delete/{uid} [delete]
func (c *userController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	strID := chi.URLParam(r, "uid")
	uid, err := strconv.Atoi(strID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(400), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Error decoding request body:" + err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	if _, err := c.userService.DeleteUser(uid); err != nil {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Message: "User deleted successfully!",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)
}

// Login log user in if they have valid credential
// @tags user-manager-apis
// @Summary log user in
// @Description log user in
// @Accept json
// @Produce json
// @Param LoginPayload body model.UserPayload true "username & password"
// @Success 200
// @Router /user/login [post]
func (c *userController) Login(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.LoginResponse
	var loginDetail model.UserPayload

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&loginDetail); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(400), 400)
		jsonResponse = &model.LoginResponse{
			Token:        "",
			RefreshToken: "",
			Message:      "Bad request: " + err.Error(),
			Code:         "400",
			Success:      false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	user, token, refreshToken, err := c.userService.LoginRequest(loginDetail.Username, loginDetail.Password)
	if err != nil {
		jsonResponse = &model.LoginResponse{
			Token:        token,
			RefreshToken: refreshToken,
			Message:      "Wrong username or password. Info:" + err.Error(),
			Code:         "400",
			Success:      false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	//user da bi xoa
	if user.DeletedAt != nil {
		jsonResponse = &model.LoginResponse{
			Token:        "user has been deleted!",
			RefreshToken: "user has been deleted!",
			Message:      "user has been deleted!",
			Code:         "406",
			Success:      true,
		}
	} else {
		jsonResponse = &model.LoginResponse{
			Token:        token,
			RefreshToken: refreshToken,
			UserId:       user.Id,
			Role:         user.Role,
			Username:     user.Username,
			LocationName: user.LocationName,
			Permission:   user.Permission,
			Progress:     user.Progress,
			Message:      "Logged in successfully as " + user.Role,
			Code:         "200",
			Success:      true,
		}
	}

	render.JSON(w, r, jsonResponse)
}

// LoginWithToken provides token each login attempt
// @tags user-manager-apis
// @Summary login user
// @Description login user, return new token string jwt
// @Accept json
// @Produce json
// @Param TokenPayload body controller.TokenPayload true "Insert your access token"
// @Success 200 {object} model.LoginResponse
// @Router /user/login/jwt [post]
func (c *userController) LoginWithToken(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.LoginResponse
	var refToken TokenPayload

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&refToken); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(400), 400)
		jsonResponse = &model.LoginResponse{
			Token:   "",
			Message: err.Error(),
			Code:    "400",
			Success: false,
		}
	}

	user, accessToken, refreshToken, success, err := c.userService.LoginWithToken(refToken.RefreshToken)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		jsonResponse = &model.LoginResponse{
			Token:   "",
			Message: err.Error(),
			Code:    "401",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	if !success {
		jsonResponse = &model.LoginResponse{
			Token:   accessToken,
			Message: err.Error(),
			Code:    "400",
			Success: false,
		}
	} else {
		jsonResponse = &model.LoginResponse{
			Token:        accessToken,
			RefreshToken: refreshToken,
			UserId:       user.Id,
			Username:     user.Username,
			LocationName: user.LocationName,
			Role:         user.Role,
			Permission:   user.Permission,
			Progress:     user.Progress,
			Message:      "jwt login successful!",
			Code:         "200",
			Success:      true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// SetPermission sets CRUD permission/startTime/endTime for username
// @tags user-manager-apis
// @Summary set permissions
// @Description input permissions/receiverUsername/starttime/endtime => user info
// @Accept json
// @Produce json
// @Param PermissionAndTimePayload body model.SetPermissionPayload true "permission payload info"
// @Security ApiKeyAuth
// @Success 200 {object} model.CreateResponse
// @Router /user/setPerm [put]
func (c *userController) SetPermission(w http.ResponseWriter, r *http.Request) {
	var userResponse *model.CreateResponse
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		userResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, userResponse)
		return
	}
	if !user.Permission {
		w.WriteHeader(http.StatusForbidden)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		userResponse = &model.CreateResponse{
			Message: "Your permission is not granted(false). Contact your administrator.",
			Success: false,
		}
		render.JSON(w, r, userResponse)
	}
	var permissionPayload model.SetPermissionPayload
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&permissionPayload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		userResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, userResponse)
		return
	}

	receiverUser, err := c.userService.GetByUsername(permissionPayload.ReceiverUsername)
	if err != nil {
		userResponse = &model.CreateResponse{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, userResponse)
		return
	}
	if (user.Role == "A1" && receiverUser.Role == "A2") || (user.Role == "A2" && receiverUser.Role == "A3") || (user.Role == "A3" && receiverUser.Role == "B1") || (user.Role == "B1" && receiverUser.Role == "B2") {
		// can change permissions
		startTime, err := utils.StringToTimeConverter(permissionPayload.StartTime)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, err.Error(), 400)
			userResponse = &model.CreateResponse{
				Message: err.Error(),
				Success: false,
			}
			render.JSON(w, r, userResponse)
			return
		}
		endTime, err := utils.StringToTimeConverter(permissionPayload.EndTime)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			http.Error(w, err.Error(), 400)
			userResponse = &model.CreateResponse{
				Message: err.Error(),
				Success: false,
			}
			render.JSON(w, r, userResponse)
			return
		}

		t := time.Now()
		splitTimeNow := strings.Split(t.String(), " ")
		nowTimeString := splitTimeNow[0] + "T" + splitTimeNow[1] + "Z"
		nowTime, _ := utils.StringToTimeConverter(nowTimeString)

		check := utils.InTimeSpan(*startTime, *endTime, *nowTime)
		var permission bool
		if check {
			permission = true
		} else {
			permission = false
			changedChildErr := c.userService.FalsePermissionToChild(receiverUser.Username)
			if changedChildErr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				userResponse = &model.CreateResponse{
					Message: err.Error(),
					Success: false,
				}
				render.JSON(w, r, userResponse)
				return
			}
		}
		changedUser, err := c.userService.SetPermission(permission, permissionPayload.ReceiverUsername, startTime, endTime)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			userResponse = &model.CreateResponse{
				Message: err.Error(),
				Success: false,
			}
		} else {
			userResponse = &model.CreateResponse{
				Id:           changedUser.Id,
				Username:     changedUser.Username,
				LocationName: changedUser.LocationName,
				Role:         changedUser.Role,
				Permission:   changedUser.Permission,
				Message:      "OK",
				Success:      true,
			}
		}
		render.JSON(w, r, userResponse)
		return
	} else {
		w.WriteHeader(http.StatusForbidden)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		userResponse = &model.CreateResponse{
			Message: "Role conflict: can't use user role " + user.Role + " to set permission for user role: " + receiverUser.Role,
			Success: false,
		}
		render.JSON(w, r, userResponse)
		return
	}

}

// GetChildUser gets all user's children users in table "users"
// @tags user-manager-apis
// @Summary get user's children users
// @Description no input => []model.User
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/getchild [get]
func (c *userController) GetChildUser(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	var childUsers []model.UserResponse
	childUsers, err = c.userService.GetChildUser(user.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    childUsers,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// GetChildCitizen gets all user's citizens
// @tags user-manager-apis
// @Summary get user's children citizens
// @Description no input => []model.Citizen
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/getcitizen [get]
func (c *userController) GetChildCitizen(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	var childCitizens []model.Citizen
	if user.Role == "A1" {
		childCitizens, err = c.citizenService.GetAll()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			jsonResponse = &model.Response{
				Message: err.Error(),
				Success: false,
			}
		} else {
			jsonResponse = &model.Response{
				Data:    childCitizens,
				Message: "OK",
				Success: true,
			}
		}
	} else {
		childCitizens, err = c.userService.GetChildCitizen(user.Username)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			jsonResponse = &model.Response{
				Message: err.Error(),
				Success: false,
			}
		} else {
			jsonResponse = &model.Response{
				Data:    childCitizens,
				Message: "OK",
				Success: true,
			}
		}
	}

	render.JSON(w, r, jsonResponse)
}

// GetCensusProgress gets census progress of account
// @tags user-manager-apis
// @Summary get user's census progress, everything that the user is managing.
// @Description no input => interface{}, output depends on which account is logged in
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/progress [get]
func (c *userController) GetCensusProgress(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	locationInfo, err := c.userService.GetCensusProgress(user.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    locationInfo,
			Message: "OK",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)
}

// SetProgress set progress for user
// @tags user-manager-apis
// @Summary set progress for user
// @Description input user id + progress (0, 1, 2) => user
// @Accept json
// @Produce json
// @Param IdAndProgressInfo body model.IdProgressPayload true "payload info"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/set_progress [put]
func (c *userController) SetProgress(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var idAndProgress model.IdProgressPayload
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&idAndProgress); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	userResponse, err := c.userService.SetProgress(idAndProgress.Username, idAndProgress.Progress)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    userResponse,
			Message: "OK",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)

}

// GetSexChart gets sex chart data of logged in user
// @tags user-manager-apis
// @Summary get data for sex chart
// @Description give out Male/Female counts
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/sex_chart [get]
func (c *userController) GetSexChart(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	sexChartData, err := c.userService.GetSexChart(user.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		arr := make([]int, 2)
		arr[0] = sexChartData.Male
		arr[1] = sexChartData.Female
		jsonResponse = &model.Response{
			Data:    arr,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetAgeChart gets age chart data of logged in user
// @tags user-manager-apis
// @Summary get data for age chart
// @Description give out Kid/Worker/Elder counts
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /user/age_chart [get]
func (c *userController) GetAgeChart(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	ageChartData, err := c.userService.GetAgeChart(user.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		arr := make([]int, 3)
		arr[0] = ageChartData.Kid
		arr[1] = ageChartData.Worker
		arr[2] = ageChartData.Elder
		jsonResponse = &model.Response{
			Data:    arr,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}
func NewUserController() UserController {
	return &userController{
		userService:     service.NewUserService(),
		provinceService: service.NewProvinceService(),
		districtService: service.NewDistrictService(),
		wardService:     service.NewWardService(),
		villageService:  service.NewVillageService(),
		citizenService:  service.NewCitizenService(),
	}
}

type TokenPayload struct {
	RefreshToken string `json:"refreshToken"`
}

type PermissionUserPayload struct {
	Permission       string `json:"permission"`
	ReceiverUsername string `json:"receiverUsername"`
}
