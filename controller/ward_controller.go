package controller

import (
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type wardController struct {
	wardService service.WardService
}

type WardController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByWardCode(w http.ResponseWriter, r *http.Request)
	GetByWardName(w http.ResponseWriter, r *http.Request)
	CreateWard(w http.ResponseWriter, r *http.Request)
	DeleteByWardId(w http.ResponseWriter, r *http.Request)
	DeleteByWardCode(w http.ResponseWriter, r *http.Request)
}

// GetAll gets all wards in the database
// @tags wards-api-manager
// @summary get all wards in the database
// @description: no input => []ward
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /ward/all [get]
func (c *wardController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	wards, err := c.wardService.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.WriteHeader(http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    wards,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetById gets all wards in the database with given id
// @tags wards-api-manager
// @summary get all wards in the database
// @description: input ward_id => ward
// @Accept json
// @Produce json
// @Param id path integer true "ward's id"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /ward/{id} [get]
func (c *wardController) GetById(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	ward, err := c.wardService.GetById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    ward,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// GetByWardCode gets all wards in the database with given code
// @tags wards-api-manager
// @summary get all wards in the database with given code
// @description: input ward_code => ward
// @Accept json
// @Produce json
// @Param code query string true "ward's code"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /ward/wcode [get]
func (c *wardController) GetByWardCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward code is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	ward, err := c.wardService.GetByWardCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    ward,
			Message: "Successfully retrieved",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByWardName gets all wards in the database with given name
// @tags wards-api-manager
// @summary get all wards in the database with given name
// @description: input ward_name => ward
// @Accept json
// @Produce json
// @Param name query string true "ward name"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /ward/wn [get]
func (c *wardController) GetByWardName(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	name := r.URL.Query().Get("name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	ward, err := c.wardService.GetByWardName(name)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    ward,
			Message: "Successfully retrieved",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)

}

// CreateWard creates a new ward with given data
// @tags wards-api-manager
// @Summary Create a new ward with given data
// @Description Ward data => new Ward in db
// @Accept json
// @Produce json
// @Param WardInfo body model.Ward true "Ward information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /ward/create [post]
func (c *wardController) CreateWard(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var newWard model.Ward
	token := r.Header.Get("Authorization")
	user, _ := middleware.GetClaimsData(token)

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newWard); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	newWard.DistrictCode = user.Username
	ward, err := c.wardService.CreateWard(&newWard)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    ward,
			Message: "Successfully created",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// DeleteByWardId deletes ward(s) with given id
// @tags wards-api-manager
// @Summary Delete ward(s) with given id
// @Description input ward_id => delete ward with that ward_id in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param id path integer true "to be deleted ward's id"
// @Success 200 {object} model.Response
// @Router /ward/delete_id/{id} [delete]
func (c *wardController) DeleteByWardId(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err = c.wardService.DeleteByWardId(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// DeleteByWardCode deletes ward(s) with given code
// @tags wards-api-manager
// @Summary Delete ward(s) with given code
// @Description input ward code => delete ward with that ward code in db
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param code query string true "to be deleted ward's id"
// @Success 200 {object} model.Response
// @Router /ward/delete_code [delete]
func (c *wardController) DeleteByWardCode(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	code := r.URL.Query().Get("code")
	if code == "" {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, http.StatusText(http.StatusBadRequest), 400)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "ward name is required",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	err := c.wardService.DeleteByWardCode(code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Data:    nil,
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    nil,
			Message: "Successfully deleted",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

func NewWardController() WardController {
	return &wardController{
		wardService: service.NewWardService(),
	}
}
