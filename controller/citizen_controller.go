package controller

import (
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/service"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type citizenController struct {
	citizenService service.CitizenService
}

type CitizenController interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByIdentityNumber(w http.ResponseWriter, r *http.Request)
	CreateCitizen(w http.ResponseWriter, r *http.Request)
	DeleteCitizen(w http.ResponseWriter, r *http.Request)
	UpdateCitizen(w http.ResponseWriter, r *http.Request)
}

// GetAll gets all Citizens in database
// @tags citizens-manager-apis
// @summary Get all citizens
// @description input: nothing => []citizen
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/all [get]
func (c *citizenController) GetAll(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	citizens, err := c.citizenService.GetAll()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    citizens,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetById gets a Citizen with the given id in db
// @tags citizens-manager-apis
// @summary Get citizen with id(PK)
// @description input: id => citizen
// @Accept json
// @Produce json
// @Param id path integer true "Citizen's id in DB, not Identity Number(CCCD)"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/id-get/{id} [get]
func (c *citizenController) GetById(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strId := chi.URLParam(r, "id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	log.Println("ID controller: ", id)
	citizen, err := c.citizenService.GetById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
	} else {
		jsonResponse = &model.Response{
			Data:    citizen,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// GetByIdentityNumber gets a Citizen with the given CCCD in db
// @tags citizens-manager-apis
// @summary Get citizen with CCCD
// @description input: CCCD => citizen
// @Accept json
// @Produce json
// @Param ident path integer true "Citizen's CCCD in DB"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/ident-get/{ident} [get]
func (c *citizenController) GetByIdentityNumber(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	strIdent := chi.URLParam(r, "ident")
	ident, err := strconv.Atoi(strIdent)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	citizen, err := c.citizenService.GetByIdentityNumber(ident)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    citizen,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}

// CreateCitizen create a Citizen in db with given information
// @tags citizens-manager-apis
// @summary Create Citizen with input
// @description input: id => citizen
// @Accept json
// @Produce json
// @Param NewCitizenInfo body model.CitizenPayload true "Citizen information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/create [post]
func (c *citizenController) CreateCitizen(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	var newCitizenPayload model.CitizenPayload
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	if user.Role != "B1" && user.Role != "B2" {
		w.WriteHeader(http.StatusForbidden)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		jsonResponse = &model.Response{
			Message: "Only users with role B1/B2 can create citizen",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newCitizenPayload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	newCitizenPayload.InputUsername = user.Username
	citizen, err := c.citizenService.CreateCitizen(&newCitizenPayload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    citizen,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)

}

// DeleteCitizen delete a Citizen in db with given information
// @tags citizens-manager-apis
// @summary Delete Citizen with id
// @description input: id => citizen
// @Accept json
// @Produce json
// @Param id path integer true "Citizen id"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/delete/{id} [delete]
func (c *citizenController) DeleteCitizen(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response
	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	if !user.Permission {
		w.WriteHeader(http.StatusForbidden)
		http.Error(w, "You do not have permission to delete", http.StatusForbidden)
		jsonResponse = &model.Response{
			Message: "You do not have permission to delete",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	strId := chi.URLParam(r, "id")
	log.Println(strId)
	id, err := strconv.Atoi(strId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}
	err = c.citizenService.DeleteCitizen(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Message: "Success",
			Success: true,
		}
	}

	render.JSON(w, r, jsonResponse)
}

// UpdateCitizen Update a Citizen in db with given information
// @tags citizens-manager-apis
// @summary Update Citizen with input
// @description input: model.Citizen(need id) => citizen
// @Accept json
// @Produce json
// @Param CitizenInfo body model.Citizen true "Citizen information"
// @Security ApiKeyAuth
// @Success 200 {object} model.Response
// @Router /citizen/update [put]
func (c *citizenController) UpdateCitizen(w http.ResponseWriter, r *http.Request) {
	var jsonResponse *model.Response

	token := r.Header.Get("Authorization")
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	if !user.Permission {
		w.WriteHeader(http.StatusForbidden)
		http.Error(w, "You do not have permission to update", http.StatusForbidden)
		jsonResponse = &model.Response{
			Message: "You do not have permission to update",
			Success: false,
		}
		render.JSON(w, r, jsonResponse)
		return
	}

	var citizenInfo model.Citizen
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&citizenInfo); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		http.Error(w, err.Error(), http.StatusBadRequest)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}

		render.JSON(w, r, jsonResponse)
		return
	}
	citizenInfo.InputUsername = user.Username
	citizen, err := c.citizenService.UpdateCitizen(citizenInfo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		jsonResponse = &model.Response{
			Message: err.Error(),
			Success: false,
		}
	} else {
		jsonResponse = &model.Response{
			Data:    citizen,
			Message: "OK",
			Success: true,
		}
	}
	render.JSON(w, r, jsonResponse)
}
func NewCitizenController() CitizenController {
	return &citizenController{
		citizenService: service.NewCitizenService(),
	}
}
