# CitizenV BE
- Prerequisites: 
    - Go 1.16 or newer
    - PostgreSQL client/server installed

## Build database docs
- **dbdocs build database.dbml -project=CitizenV**
## Command run program in local
- Command: **go run main.go -db true**
- Args: 
    - db: allow initialize db while start program | type of *boolean* | default value is *false*

## Swagger for dev before run air
**swag init -g *$file_header***
- file_header is the file contain declaration host and port for swagger
    - **header_local.go**: for run in local
    - **header_production.go**: for run in production server

## Swagger with header at server
swag init 

## Git Commands: 
### Workflow: git.master -> git.production -> heroku.master
- Before work: **git pull** 
- Work: 
    - git checkout master
    - code
    - commit change to master
    - git checkout production
    - git merge master
    - commit change to production
    - deploy: git push heroku production:master 
