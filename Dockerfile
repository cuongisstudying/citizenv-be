FROM golang:1.16-alpine AS builder

# Set maintainer
LABEL Maintainer="Cuong Dang Trung <cuongisstudying@gmail.com>"

# Set HEADER AND ENV FILES
ARG HEADER_FILE
ARG ENV_FILE
ENV HEADER_FILE=header_production.go
ENV ENV_FILE=.env.pro

# Check HEADER_FILE & ENV_FILE
RUN echo "File swagger: $HEADER_FILE"
RUN echo "File env: $ENV_FILE"

RUN apk add bash ca-certificates git gcc g++ libc-dev

# Set WORKDIR
RUN mkdir -p /work/citizenv
WORKDIR /work/citizenv

# Copy go.mod and go.sum
COPY go.mod .
COPY go.sum .
RUN ls -la /work/citizenv/

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# COPY everything else
COPY . /work/citizenv/
# COPY $ENV_FILE /work/citizenv/.env

RUN go get -u github.com/swaggo/swag/cmd/swag
RUN swag init --parseDependency -g header_production.go

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o citizenv .

# ######## Start a new stage from scratch #######
# FROM ubuntu:20.04 

# RUN apt-get update
# RUN apt-get install -y ca-certificates

# # Here we copy the rest of the source code
# WORKDIR /usr/local/bin

# COPY --from=citizenv-builder:builder /work/citizenv/. /usr/local/bin
# # COPY . /usr/local/bin

# RUN ls -la /usr/local/bin/infrastructure

# EXPOSE 10050

CMD ["./citizenv"]
