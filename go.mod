module citizenv

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/jwtauth v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/joho/godotenv v1.4.0
	github.com/swaggo/http-swagger v1.1.2
	github.com/swaggo/swag v1.7.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gorm.io/driver/postgres v1.2.2
	gorm.io/gorm v1.22.3
)
