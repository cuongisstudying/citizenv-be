package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"

	"gorm.io/gorm"
)

type citizenRepository struct {
	provinceRepository model.ProvinceRepository
	districtRepository model.DistrictRepository
	wardRepository     model.WardRepository
}

func (r *citizenRepository) GetAll() ([]model.Citizen, error) {
	db := infrastructure.GetDB()

	var citizens []model.Citizen
	if err := db.Model(&model.Citizen{}).Find(&citizens).Error; err != nil {
		return nil, err
	}

	// //update input location
	// for i := range citizens {
	// 	cid := citizens[i].Id
	// 	inputLocation := TransInputCodeToLocation(citizens[i].InputUsername)
	// 	log.Println(inputLocation)
	// 	if err := db.Model(&model.Citizen{}).Where("id = ?", cid).Update("input_location", inputLocation).Error; err != nil {
	// 		return nil, err
	// 	}
	// }

	return citizens, nil
}

func (r *citizenRepository) GetById(id int) (*model.Citizen, error) {
	db := infrastructure.GetDB()
	var citizen model.Citizen

	if err := db.Model(&model.Citizen{}).Where("id = ?", id).Find(&citizen).Error; err != nil {
		return nil, err
	}

	return &citizen, nil
}

func (r *citizenRepository) GetByIdentityNumber(identityNumber int) (*model.Citizen, error) {
	db := infrastructure.GetDB()
	var citizen model.Citizen

	if err := db.Model(&model.Citizen{}).Where("ident_number = ?", identityNumber).Find(&citizen).Error; err != nil {
		return nil, err
	}

	return &citizen, nil
}

func (r *citizenRepository) CreateCitizen(newCitizen *model.Citizen) (*model.Citizen, error) {
	db := infrastructure.GetDB()

	inputUsername := newCitizen.InputUsername
	inputSplit := Chunks(inputUsername, 2)
	provinceCode := inputSplit[0]
	districtCode := provinceCode + inputSplit[1]
	wardCode := districtCode + inputSplit[2]
	var villageCode string
	if len(inputUsername) == 8 {
		villageCode = wardCode + inputSplit[3]
	}

	var provinceName string
	var districtName string
	var wardName string
	var villageName string

	if err := db.Model(&model.Province{}).Select("Name").Where("code = ?", provinceCode).Find(&provinceName).Error; err != nil {
		return nil, err
	}

	if err := db.Model(&model.District{}).Select("Name").Where("code = ?", districtCode).Find(&districtName).Error; err != nil {
		return nil, err
	}

	if err := db.Model(&model.Ward{}).Select("Name").Where("code = ?", wardCode).Find(&wardName).Error; err != nil {
		return nil, err
	}
	var permaAddress string
	if newCitizen.PermanentAddress == "" {
		permaAddress = wardName + ", " + districtName + ", " + provinceName
		newCitizen.PermanentAddress = permaAddress
	}

	if newCitizen.TempAddress == "" {
		tempAddress := permaAddress
		newCitizen.TempAddress = tempAddress
	}

	if villageCode != "" {
		if err := db.Model(&model.Village{}).Select("Name").Where("code = ?", villageCode).Find(&villageName).Error; err != nil {
			return nil, err
		}
		newCitizen.InputLocation = villageName + ", " + wardName + ", " + districtName + ", " + provinceName
	} else {
		newCitizen.InputLocation = wardName + ", " + districtName + ", " + provinceName
	}

	db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(&model.Citizen{}).Create(&newCitizen).Error; err != nil {
			return err
		}
		var totalInputProvince int
		var totalInputDistrict int
		var totalInputWard int
		var totalInputVillage int
		if err := tx.Model(&model.Province{}).Select("TotalInput").Where("code = ?", provinceCode).Find(&totalInputProvince).Error; err != nil {
			return err
		}
		if err := tx.Model(&model.District{}).Select("TotalInput").Where("code = ?", districtCode).Find(&totalInputDistrict).Error; err != nil {
			return err
		}
		if err := tx.Model(&model.Ward{}).Select("TotalInput").Where("code = ?", wardCode).Find(&totalInputWard).Error; err != nil {
			return err
		}
		if villageCode != "" {
			if err := tx.Model(&model.Village{}).Select("TotalInput").Where("code = ?", villageCode).Find(&totalInputVillage).Error; err != nil {
				return err
			}
		}
		totalInputProvince += 1
		totalInputDistrict += 1
		totalInputWard += 1

		if err := tx.Model(&model.Province{}).Where("code = ?", provinceCode).Update("total_input", totalInputProvince).Error; err != nil {
			return err
		}
		if err := tx.Model(&model.District{}).Where("code = ?", districtCode).Update("total_input", totalInputDistrict).Error; err != nil {
			return err
		}
		if err := tx.Model(&model.Ward{}).Where("code = ?", wardCode).Update("total_input", totalInputWard).Error; err != nil {
			return err
		}
		if villageCode != "" {
			totalInputVillage += 1
			if err := tx.Model(&model.Village{}).Where("code = ?", villageCode).Update("total_input", totalInputVillage).Error; err != nil {
				return err
			}
		}
		return nil
	})

	return newCitizen, nil
}

func (r *citizenRepository) DeleteCitizen(id int) error {
	db := infrastructure.GetDB()

	if err := db.Model(&model.Citizen{}).Delete("id =?", id).Error; err != nil {
		return err
	}

	return nil
}

func (r *citizenRepository) UpdateCitizen(citizenInfo model.Citizen) (*model.Citizen, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.Citizen{}).Where("id = ?", citizenInfo.Id).Updates(&citizenInfo).Error; err != nil {
		return nil, err
	}

	return &citizenInfo, nil
}
func NewCitizenRepository() model.CitizenRepository {
	return &citizenRepository{
		provinceRepository: NewProvinceRepository(),
		districtRepository: NewDistrictRepository(),
		wardRepository:     NewWardRepository(),
	}
}

func Chunks(s string, chunkSize int) []string {
	if len(s) == 0 {
		return nil
	}
	if chunkSize >= len(s) {
		return []string{s}
	}
	var chunks []string = make([]string, 0, (len(s)-1)/chunkSize+1)
	currentLen := 0
	currentStart := 0
	for i := range s {
		if currentLen == chunkSize {
			chunks = append(chunks, s[currentStart:i])
			currentLen = 0
			currentStart = i
		}
		currentLen++
	}
	chunks = append(chunks, s[currentStart:])
	return chunks
}

func TransInputCodeToLocation(inputUsername string) string {
	inputSplit := Chunks(inputUsername, 2)
	provinceCode := inputSplit[0]
	districtCode := provinceCode + inputSplit[1]
	wardCode := districtCode + inputSplit[2]

	var villageCode string
	if len(inputUsername) == 8 {
		villageCode = wardCode + inputSplit[3]
	}
	var provinceName string
	var districtName string
	var wardName string
	var villageName string

	db := infrastructure.GetDB()
	if err := db.Model(&model.Province{}).Select("Name").Where("code = ?", provinceCode).Find(&provinceName).Error; err != nil {
		return err.Error()
	}

	if err := db.Model(&model.District{}).Select("Name").Where("code = ?", districtCode).Find(&districtName).Error; err != nil {
		return err.Error()
	}
	if err := db.Model(&model.Ward{}).Select("Name").Where("code = ?", wardCode).Find(&wardName).Error; err != nil {
		return err.Error()
	}

	var inputLocation string
	if villageCode != "" {
		if err := db.Model(&model.Village{}).Select("Name").Where("code = ?", villageCode).Find(&villageName).Error; err != nil {
			return err.Error()
		}
		inputLocation = villageName + ", " + wardName + ", " + districtName + ", " + provinceName
	} else {
		inputLocation = wardName + ", " + districtName + ", " + provinceName
	}

	return inputLocation

}
