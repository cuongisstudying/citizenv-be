package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/utils"
	"errors"
	"log"
	"math"
	"strings"
	"time"

	"gorm.io/gorm"
)

type userRepository struct {
	provinceRepository model.ProvinceRepository
	districtRepository model.DistrictRepository
	wardRepository     model.WardRepository
	citizenRepository  model.CitizenRepository
}

func (r *userRepository) GetAll() ([]model.User, error) {
	db := infrastructure.GetDB()
	var users []model.User

	if err := db.Model(&model.User{}).Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (r *userRepository) CreateUser(newUser *model.User) (*model.User, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.User{}).Create(&newUser).Error; err != nil {
		return nil, err
	}

	return newUser, nil
}

func (r *userRepository) GetById(id int) (*model.User, error) {
	db := infrastructure.GetDB()

	var user model.User
	if err := db.First(&user, id).Error; err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *userRepository) GetByUsername(username string) (*model.User, error) {
	db := infrastructure.GetDB()

	var user model.User
	if err := db.Where("username = ?", username).First(&user).Error; err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *userRepository) DeleteUser(id int) (*model.User, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.User{Id: id}).Update("deletedAt", time.Now()).Error; err != nil {
		return nil, err
	}

	return r.GetById(id)
}

func (r *userRepository) LoginTokenRequest(user *model.User) (bool, error) {
	db := infrastructure.GetDB()

	var userInfo model.User
	if err := db.Where(&model.User{
		Username: user.Username,
		Password: user.Password,
	}).First(&userInfo).Error; err != nil {
		infrastructure.ErrLog.Println(err)
		return false, nil
	}

	user.ExpiresAt = time.Now().Local().Add(time.Hour*time.Duration(infrastructure.Extend_Hour)).UnixNano() / infrastructure.NANO_TO_SECOND
	return true, nil
}

func (r *userRepository) SetPermission(permission bool, receiverUsername string, startTime *time.Time, endTime *time.Time) (*model.User, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.User{}).Where("username = ?", receiverUsername).Update("permission", permission).Update("start_time", startTime).Update("end_time", endTime).Error; err != nil {
		return nil, err
	}

	return r.GetByUsername(receiverUsername)
}

func (r *userRepository) GetChildUser(username string) ([]model.User, error) {
	db := infrastructure.GetDB()
	if username == "A1" {
		var allUsers []model.User
		var returnUsers []model.User
		if err := db.Model(&model.User{}).Find(&allUsers).Error; err != nil {
			return nil, err
		}
		for i := range allUsers {
			if len(allUsers[i].Username) == 2 && allUsers[i].Username != "A1" {
				returnUsers = append(returnUsers, allUsers[i])
			}
		}
		return returnUsers, nil
	}

	var usernames []string
	if err := db.Model(&model.User{}).Select("username").Scan(&usernames).Error; err != nil {
		return nil, err
	}
	var childUsername []string
	lenPrefix := len(username)
	for i := range usernames {
		if strings.HasPrefix(usernames[i], username) && len(usernames[i])-2 == lenPrefix {
			childUsername = append(childUsername, usernames[i])
		}
	}
	var childUsers []model.User

	db.Transaction(func(tx *gorm.DB) error {
		for i := range childUsername {
			var txUser model.User
			if err := db.Model(&model.User{}).Where("username = ?", childUsername[i]).Find(&txUser).Error; err != nil {
				return err
			}
			childUsers = append(childUsers, txUser)
		}

		return nil
	})

	return childUsers, nil

}

func (r *userRepository) GetChildUsernameAllLevel(username string) ([]string, error) {
	db := infrastructure.GetDB()

	// get all users
	var allUsers []model.User
	if err := db.Model(&model.User{}).Find(&allUsers).Error; err != nil {
		return nil, err
	}
	// all user with prefix and len > len(username) is child; can't lock A1
	// only use lock/unlock from username string number "01..63"
	var childUsernames []string
	lenParentUsername := len(username)
	for i := range allUsers {
		if strings.HasPrefix(allUsers[i].Username, username) && len(allUsers[i].Username) > lenParentUsername {
			childUsernames = append(childUsernames, allUsers[i].Username)
		}
	}
	log.Println("get child user all: ", childUsernames)
	return childUsernames, nil

}
func (r *userRepository) GetChildCitizen(username string) ([]model.Citizen, error) {
	db := infrastructure.GetDB()

	var inputUsernames []string
	if err := db.Model(&model.Citizen{}).Select("InputUsername").Scan(&inputUsernames).Error; err != nil {
		return nil, err
	}

	var childUsername []string
	for i := range inputUsernames {
		if strings.HasPrefix(inputUsernames[i], username) {
			childUsername = append(childUsername, inputUsernames[i])
		}
	}

	remDupUsn := removeDuplicateValues(childUsername)
	var citizens []model.Citizen
	db.Transaction(func(tx *gorm.DB) error {
		for i := range remDupUsn {
			var txCitizen []model.Citizen
			if err := tx.Model(&model.Citizen{}).Distinct().Where("input_username = ?", remDupUsn[i]).Find(&txCitizen).Error; err != nil {
				return err
			}

			citizens = append(citizens, txCitizen...)
		}

		return nil
	})

	return citizens, nil
}

func (r *userRepository) GetCensusProgress(username string) (interface{}, error) {
	log.Println(username)
	if username == "A1" {
		return r.provinceRepository.GetAll()
	}
	if len(username) == 2 && username != "A1" {
		loc, err := r.provinceRepository.GetByProvinceCode(username)
		if err != nil {
			return nil, err
		}
		return loc, nil
	} else if len(username) == 4 {
		loc, err := r.districtRepository.GetByDistrictCode(username)
		if err != nil {
			return nil, err
		}
		return loc, nil
	} else if len(username) == 6 {
		loc, err := r.wardRepository.GetByWardCode(username)
		if err != nil {
			return nil, err
		}
		return loc, nil
	}

	return nil, errors.New("can't get your username maybe?")
}

// this is used for changing child user permission to true when logging in
func (r *userRepository) ChangePermissionWhenLogin(username string) error {
	db := infrastructure.GetDB()
	childUsers, err := r.GetChildUser(username)
	if err != nil {
		return err
	}
	t := time.Now()
	splitTimeNow := strings.Split(t.String(), " ")
	nowTimeString := splitTimeNow[0] + "T" + splitTimeNow[1] + "Z"
	nowTime, _ := utils.StringToTimeConverter(nowTimeString)
	for i := range childUsers {
		if childUsers[i].StartTime.IsZero() || childUsers[i].EndTime.IsZero() {
			if err := db.Model(&model.User{}).Where("username = ?", childUsers[i].Username).Update("permission", false).Error; err != nil {
				return err
			}
		} else {
			if utils.InTimeSpan(*childUsers[i].StartTime, *childUsers[i].EndTime, *nowTime) {
				if err := db.Model(&model.User{}).Where("username = ?", childUsers[i].Username).Update("permission", true).Error; err != nil {
					return err
				}
			} else {
				if err := db.Model(&model.User{}).Where("username = ?", childUsers[i].Username).Update("permission", false).Error; err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (r *userRepository) FalsePermissionToChild(username string) error {
	db := infrastructure.GetDB()
	childUsernames, err := r.GetChildUsernameAllLevel(username)
	if err != nil {
		return err
	}

	for i := range childUsernames {
		if err := db.Model(&model.User{}).Where("username = ?", childUsernames[i]).Update("permission", false).Error; err != nil {
			return err
		}
	}
	return nil

}

func (r *userRepository) SetProgress(username string, progress int) (*model.User, error) {
	db := infrastructure.GetDB()
	if err := db.Model(&model.User{}).Where("username = ?", username).Update("progress", progress).Error; err != nil {
		return nil, err
	}

	for {
		check := r.UpdateParentProgress(username, progress)
		if !check {
			break
		}
	}

	return r.GetByUsername(username)
}

func (r *userRepository) UpdateParentProgress(username string, progress int) bool {
	parentName := GetParentUsername(username)
	if parentName == "" {
		return false
	}
	childUser, _ := r.GetChildUser(parentName)
	for i := range childUser {
		if childUser[i].Progress != progress {
			return false
		}
	}
	db := infrastructure.GetDB()
	if err := db.Model(&model.User{}).Where("username = ?", parentName).Update("progress", progress).Error; err != nil {
		return false
	}

	return r.UpdateParentProgress(GetParentUsername(username), progress)

}

func (r *userRepository) GetSexChart(username string) (*model.SexChartData, error) {
	chartData := &model.SexChartData{
		Male:   0,
		Female: 0,
	}

	if username == "A1" {
		childCitizens, err := r.citizenRepository.GetAll()
		if err != nil {
			return nil, err
		}
		for i := range childCitizens {
			if childCitizens[i].Sex == "Nam" {
				chartData.Male += 1
			} else if childCitizens[i].Sex == "Nữ" {
				chartData.Female += 1
			}
		}
		return chartData, nil
	}

	childCitizens, err := r.GetChildCitizen(username)
	if err != nil {
		return nil, err
	}
	for i := range childCitizens {
		if childCitizens[i].Sex == "Nam" {
			chartData.Male += 1
		} else if childCitizens[i].Sex == "Nữ" {
			chartData.Female += 1
		}
	}

	return chartData, nil
}

func (r *userRepository) GetAgeChart(username string) (*model.AgeChartData, error) {
	ageData := &model.AgeChartData{
		Kid:    0,
		Worker: 0,
		Elder:  0,
	}
	if username == "A1" {
		childCitizens, err := r.citizenRepository.GetAll()
		if err != nil {
			return nil, err
		}
		for i := range childCitizens {
			timeNow := utils.NowTimeConverter()
			diff := timeNow.Sub(*childCitizens[i].Birthday)
			year := RoundTime(diff.Seconds() / 31207680)
			if year <= 14 {
				ageData.Kid += 1
			} else if year >= 15 && year <= 59 {
				ageData.Worker += 1
			} else {
				ageData.Elder += 1
			}

		}
		return ageData, nil
	}
	childCitizens, err := r.GetChildCitizen(username)
	if err != nil {
		return nil, err
	}
	for i := range childCitizens {
		timeNow := utils.NowTimeConverter()
		diff := timeNow.Sub(*childCitizens[i].Birthday)
		year := RoundTime(diff.Seconds() / 31207680)
		if year <= 14 {
			ageData.Kid += 1
		} else if year >= 15 && year <= 59 {
			ageData.Worker += 1
		} else {
			ageData.Elder += 1
		}

	}
	return ageData, nil

}
func NewUserRepository() model.UserRepository {
	return &userRepository{
		provinceRepository: NewProvinceRepository(),
		districtRepository: NewDistrictRepository(),
		wardRepository:     NewWardRepository(),
		citizenRepository:  NewCitizenRepository(),
	}
}

func GetParentUsername(childUsername string) string {
	if childUsername == "A1" {
		return ""
	} else if len(childUsername) == 2 {
		return "A1"
	}
	if len(childUsername) == 4 {
		return childUsername[0:2]
	}
	if len(childUsername) == 6 {
		return childUsername[0:4]
	}
	if len(childUsername) == 8 {
		return childUsername[0:6]
	}
	return ""
}

func removeDuplicateValues(stringSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range stringSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}

	return list
}

func RoundTime(input float64) int {
	var result float64

	if input < 0 {
		result = math.Ceil(input - 0.5)
	} else {
		result = math.Floor(input + 0.5)
	}

	// only interested in integer, ignore fractional
	i, _ := math.Modf(result)

	return int(i)
}
