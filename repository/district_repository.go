package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"
)

type districtRepository struct{}

func (r *districtRepository) GetAll() ([]model.District, error) {
	db := infrastructure.GetDB()
	var districts []model.District

	if err := db.Model(&model.District{}).Preload("Wards.Villages").Find(&districts).Error; err != nil {
		return nil, err
	}

	return districts, nil
}

func (r *districtRepository) GetById(id int) (*model.District, error) {
	db := infrastructure.GetDB()
	var district model.District

	if err := db.Model(&model.District{}).Where("id = ?", id).Preload("Wards.Villages").Find(&district).Error; err != nil {
		return nil, err
	}

	return &district, nil
}

func (r *districtRepository) GetByDistrictCode(code string) (*model.District, error) {
	db := infrastructure.GetDB()
	var district model.District

	if err := db.Model(&model.District{}).Where("code = ?", code).Preload("Wards.Villages").Find(&district).Error; err != nil {
		return nil, err
	}

	return &district, nil
}

func (r *districtRepository) GetByDistrictName(name string) (*model.District, error) {
	db := infrastructure.GetDB()
	var district model.District

	if err := db.Model(&model.District{}).Where("name = ?", name).Preload("Wards.Villages").Find(&district).Error; err != nil {
		return nil, err
	}

	return &district, nil
}
func (r *districtRepository) CreateDistrict(newDistrict *model.District) (*model.District, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.District{}).Create(newDistrict).Error; err != nil {
		return nil, err
	}

	return newDistrict, nil
}

func (r *districtRepository) DeleteByDistrictId(id int) error {
	db := infrastructure.GetDB()

	var record model.District
	if err := db.Model(&model.District{}).Where("id = ?", id).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func (r *districtRepository) DeleteByDistrictCode(code string) error {
	db := infrastructure.GetDB()

	var record model.District
	if err := db.Model(&model.District{}).Where("code = ?", code).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func NewDistrictRepository() model.DistrictRepository {
	return &districtRepository{}
}
