package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"
)

type wardRepository struct{}

func (r *wardRepository) GetAll() ([]model.Ward, error) {
	db := infrastructure.GetDB()
	var wards []model.Ward

	if err := db.Model(&model.Ward{}).Preload("Villages").Find(&wards).Error; err != nil {
		return nil, err
	}

	return wards, nil
}

func (r *wardRepository) GetById(id int) (*model.Ward, error) {
	db := infrastructure.GetDB()
	var ward model.Ward

	if err := db.Model(&model.Ward{}).Where("id = ?", id).Preload("Villages").Find(&ward).Error; err != nil {
		return nil, err
	}

	return &ward, nil
}

func (r *wardRepository) GetByWardCode(code string) (*model.Ward, error) {
	db := infrastructure.GetDB()
	var ward model.Ward

	if err := db.Model(&model.Ward{}).Where("code = ?", code).Preload("Villages").Find(&ward).Error; err != nil {
		return nil, err
	}

	return &ward, nil
}

func (r *wardRepository) GetByWardName(name string) (*model.Ward, error) {
	db := infrastructure.GetDB()
	var ward model.Ward

	if err := db.Model(&model.Ward{}).Where("name = ?", name).Preload("Villages").Find(&ward).Error; err != nil {
		return nil, err
	}

	return &ward, nil
}
func (r *wardRepository) CreateWard(newWard *model.Ward) (*model.Ward, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.Ward{}).Create(newWard).Error; err != nil {
		return nil, err
	}

	return newWard, nil
}

func (r *wardRepository) DeleteByWardId(id int) error {
	db := infrastructure.GetDB()
	var record model.Ward
	if err := db.Model(&model.Ward{}).Where("id = ?", id).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func (r *wardRepository) DeleteByWardCode(code string) error {
	db := infrastructure.GetDB()

	var record model.Ward
	if err := db.Model(&model.Ward{}).Where("code = ?", code).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func NewWardRepository() model.WardRepository {
	return &wardRepository{}
}
