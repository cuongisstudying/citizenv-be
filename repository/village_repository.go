package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"
)

type villageRepository struct{}

func (r *villageRepository) GetAll() ([]model.Village, error) {
	db := infrastructure.GetDB()
	var villages []model.Village

	if err := db.Model(&model.Village{}).Find(&villages).Error; err != nil {
		return nil, err
	}

	return villages, nil
}

func (r *villageRepository) GetById(id int) (*model.Village, error) {
	db := infrastructure.GetDB()
	var village model.Village

	if err := db.Model(&model.Village{}).Where("id = ?", id).Find(&village).Error; err != nil {
		return nil, err
	}

	return &village, nil
}

func (r *villageRepository) GetByVillageCode(code string) (*model.Village, error) {
	db := infrastructure.GetDB()
	var village model.Village

	if err := db.Model(&model.Village{}).Where("code = ?", code).Find(&village).Error; err != nil {
		return nil, err
	}

	return &village, nil
}

func (r *villageRepository) GetByVillageName(name string) (*model.Village, error) {
	db := infrastructure.GetDB()
	var village model.Village

	if err := db.Model(&model.Village{}).Where("name = ?", name).Find(&village).Error; err != nil {
		return nil, err
	}

	return &village, nil
}
func (r *villageRepository) CreateVillage(newVillage *model.Village) (*model.Village, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.Village{}).Create(newVillage).Error; err != nil {
		return nil, err
	}

	return newVillage, nil
}

func (r *villageRepository) DeleteByVillageId(id int) error {
	db := infrastructure.GetDB()

	var record model.Village
	if err := db.Model(&model.Village{}).Where("id = ?", id).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func (r *villageRepository) DeleteByVillageCode(code string) error {
	db := infrastructure.GetDB()

	var record model.Village
	if err := db.Model(&model.Village{}).Where("code = ?", code).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func NewVillageRepository() model.VillageRepository {
	return &villageRepository{}
}
