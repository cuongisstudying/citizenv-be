package repository

import (
	"citizenv/infrastructure"
	"citizenv/model"
)

type provinceRepository struct{}

func (r *provinceRepository) GetAll() ([]model.Province, error) {
	db := infrastructure.GetDB()
	var provinces []model.Province

	if err := db.Model(&model.Province{}).Preload("Districts.Wards.Villages").Find(&provinces).Error; err != nil {
		return nil, err
	}

	return provinces, nil
}

func (r *provinceRepository) GetById(id int) (*model.Province, error) {
	db := infrastructure.GetDB()
	var province model.Province

	if err := db.Model(&model.Province{}).Where("id = ?", id).Preload("Districts.Wards.Villages").Find(&province).Error; err != nil {
		return nil, err
	}

	return &province, nil
}

func (r *provinceRepository) GetByProvinceCode(code string) (*model.Province, error) {
	db := infrastructure.GetDB()
	var province model.Province

	if err := db.Model(&model.Province{}).Where("code = ?", code).Preload("Districts.Wards.Villages").Find(&province).Error; err != nil {
		return nil, err
	}

	return &province, nil
}

func (r *provinceRepository) GetByProvinceName(name string) (*model.Province, error) {
	db := infrastructure.GetDB()
	var province model.Province

	if err := db.Model(&model.Province{}).Where("name = ?", name).Preload("Districts.Wards.Villages").Find(&province).Error; err != nil {
		return nil, err
	}

	return &province, nil
}
func (r *provinceRepository) CreateProvince(newProvince *model.Province) (*model.Province, error) {
	db := infrastructure.GetDB()

	if err := db.Model(&model.Province{}).Create(newProvince).Error; err != nil {
		return nil, err
	}

	return newProvince, nil
}

func (r *provinceRepository) DeleteByProvinceId(id int) error {
	db := infrastructure.GetDB()

	var record model.Province
	if err := db.Model(&model.Province{}).Where("id = ?", id).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func (r *provinceRepository) DeleteByProvinceCode(code string) error {
	db := infrastructure.GetDB()

	var record model.Province
	if err := db.Model(&model.Province{}).Where("code = ?", code).Delete(&record).Error; err != nil {
		return err
	}

	return nil
}

func NewProvinceRepository() model.ProvinceRepository {
	return &provinceRepository{}
}
