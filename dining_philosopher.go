package main

import (
	"fmt"
	"sync"
	"time"
)

type Chopstick struct {
	sync.Mutex
}
type Philosopher struct {
	number     int
	count      int
	leftchops  *Chopstick
	rightchops *Chopstick
}

func (p Philosopher) Eat(c chan *Philosopher, wg *sync.WaitGroup) {
	for i := 0; i < 3; i++ {
		c <- &p
		if p.count < 3 {
			p.leftchops.Lock()
			p.rightchops.Lock()

			fmt.Println("starting to eat", p.number)

			p.count += 1

			fmt.Println("finishing eating", p.number)
			p.rightchops.Unlock()
			p.leftchops.Unlock()

			wg.Done()
		}

	}
}

func Host(c chan *Philosopher, wg *sync.WaitGroup) {
	for {
		if len(c) == 2 {
			<-c
			<-c

			time.Sleep(80 * time.Millisecond)
		}
	}
}

func main() {
	var i int
	var wg sync.WaitGroup
	c := make(chan *Philosopher, 2)

	wg.Add(15)

	Chopsticks := make([]*Chopstick, 5)
	for i = 0; i < 5; i++ {
		Chopsticks[i] = new(Chopstick)
	}

	Philosophers := make([]*Philosopher, 5)
	for i = 0; i < 5; i++ {
		Philosophers[i] = &Philosopher{i + 1, 0, Chopsticks[i], Chopsticks[(i+1)%5]}
	}

	go Host(c, &wg)
	for i = 0; i < 5; i++ {
		go Philosophers[i].Eat(c, &wg)
	}
	wg.Wait()
}
