package service

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/repository"
)

type wardService struct {
	wardRepository model.WardRepository
}

type WardService interface {
	GetAll() ([]model.Ward, error)
	GetById(id int) (*model.Ward, error)
	GetByWardCode(code string) (*model.Ward, error)
	GetByWardName(name string) (*model.Ward, error)
	CreateWard(newWard *model.Ward) (*model.Ward, error)
	DeleteByWardId(id int) error
	DeleteByWardCode(code string) error
}

func (s *wardService) GetAll() ([]model.Ward, error) {
	return s.wardRepository.GetAll()
}

func (s *wardService) GetById(id int) (*model.Ward, error) {
	ward, err := s.wardRepository.GetById(id)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return ward, nil
}

func (s *wardService) GetByWardCode(code string) (*model.Ward, error) {
	ward, err := s.wardRepository.GetByWardCode(code)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return ward, nil
}

func (s *wardService) GetByWardName(name string) (*model.Ward, error) {
	ward, err := s.wardRepository.GetByWardName(name)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return ward, nil
}

func (s *wardService) CreateWard(newWard *model.Ward) (*model.Ward, error) {
	newWard, err := s.wardRepository.CreateWard(newWard)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return newWard, err
}

func (s *wardService) DeleteByWardId(id int) error {
	if err := s.wardRepository.DeleteByWardId(id); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func (s *wardService) DeleteByWardCode(code string) error {
	if err := s.wardRepository.DeleteByWardCode(code); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func NewWardService() WardService {
	return &wardService{
		wardRepository: repository.NewWardRepository(),
	}
}
