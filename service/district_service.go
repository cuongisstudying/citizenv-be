package service

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/repository"
)

type districtService struct {
	districtRepository model.DistrictRepository
}

type DistrictService interface {
	GetAll() ([]model.District, error)
	GetById(id int) (*model.District, error)
	GetByDistrictCode(code string) (*model.District, error)
	GetByDistrictName(name string) (*model.District, error)
	CreateDistrict(newDistrict *model.District) (*model.District, error)
	DeleteByDistrictId(id int) error
	DeleteByDistrictCode(code string) error
}

func (s *districtService) GetAll() ([]model.District, error) {
	return s.districtRepository.GetAll()
}

func (s *districtService) GetById(id int) (*model.District, error) {
	district, err := s.districtRepository.GetById(id)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return district, nil
}

func (s *districtService) GetByDistrictCode(code string) (*model.District, error) {
	district, err := s.districtRepository.GetByDistrictCode(code)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return district, nil
}

func (s *districtService) GetByDistrictName(name string) (*model.District, error) {
	district, err := s.districtRepository.GetByDistrictName(name)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return district, nil
}

func (s *districtService) CreateDistrict(newDistrict *model.District) (*model.District, error) {
	newDistrict, err := s.districtRepository.CreateDistrict(newDistrict)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return newDistrict, err
}

func (s *districtService) DeleteByDistrictId(id int) error {
	if err := s.districtRepository.DeleteByDistrictId(id); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func (s *districtService) DeleteByDistrictCode(code string) error {
	if err := s.districtRepository.DeleteByDistrictCode(code); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func NewDistrictService() DistrictService {
	return &districtService{
		districtRepository: repository.NewDistrictRepository(),
	}
}
