package service

import (
	"citizenv/infrastructure"
	"citizenv/middleware"
	"citizenv/model"
	"citizenv/repository"
	"errors"
	"log"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	GetAll() ([]model.UserResponse, error)
	CreateUser(newUser *model.User) (*model.UserResponse, error)
	GetById(id int) (*model.UserResponse, error)
	GetByUsername(username string) (*model.UserResponse, error)
	DeleteUser(id int) (*model.User, error)
	CheckCredentials(id int, password string) (*model.User, error)
	LoginRequest(username string, password string) (*model.User, string, string, error)
	LoginWithToken(token string) (*model.User, string, string, bool, error)
	SetPermission(permission bool, receiverUsername string, startTime *time.Time, endTime *time.Time) (*model.UserResponse, error)
	GetChildUser(username string) ([]model.UserResponse, error)
	GetChildCitizen(username string) ([]model.Citizen, error)
	GetCensusProgress(username string) (interface{}, error)
	ChangePermissionOnly(username string) error
	FalsePermissionToChild(username string) error
	SetProgress(username string, progress int) (*model.UserResponse, error)
	GetSexChart(username string) (*model.SexChartData, error)
	GetAgeChart(username string) (*model.AgeChartData, error)
}

type userService struct {
	userRepository model.UserRepository
}

func (s *userService) GetAll() ([]model.UserResponse, error) {
	users, err := s.userRepository.GetAll()
	if err != nil {
		infrastructure.ErrLog.Println(err)
		return nil, err
	}

	var userResponseList []model.UserResponse
	for i := range users {
		index := model.UserResponse{
			Id:           users[i].Id,
			Username:     users[i].Username,
			Role:         users[i].Role,
			Permission:   users[i].Permission,
			LocationName: users[i].LocationName,
			StartTime:    users[i].StartTime,
			EndTime:      users[i].EndTime,
			Progress:     users[i].Progress,
		}
		userResponseList = append(userResponseList, index)
	}

	return userResponseList, nil
}

func (s *userService) CreateUser(newUser *model.User) (*model.UserResponse, error) {
	newUser.Password = hashAndSalt(newUser.Password)
	newUser, err := s.userRepository.CreateUser(newUser)
	if err != nil {
		return nil, err
	}

	newUserResponse := model.UserResponse{
		Id:           newUser.Id,
		Username:     newUser.Username,
		Role:         newUser.Role,
		LocationName: newUser.LocationName,
	}
	return &newUserResponse, nil
}

func (s *userService) GetById(id int) (*model.UserResponse, error) {
	user, err := s.userRepository.GetById(id)
	if err != nil {
		return nil, err
	}

	return &model.UserResponse{
		Id:           user.Id,
		Username:     user.Username,
		Role:         user.Role,
		Permission:   user.Permission,
		LocationName: user.LocationName,
		StartTime:    user.StartTime,
		EndTime:      user.EndTime,
		Progress:     user.Progress,
	}, nil

}

func (s *userService) DeleteUser(id int) (*model.User, error) {
	return s.userRepository.DeleteUser(id)
}

func (s *userService) LoginRequest(username string, password string) (*model.User, string, string, error) {
	// validate username/password
	user, err := s.userRepository.GetByUsername(username)
	if err != nil {
		return nil, "", "", err
	}

	err = checkPassword(user, password)
	if err != nil {
		return nil, "", "", err
	}

	// get JWT
	tokenString, refreshToken, err := middleware.GetTokenString(user)
	if err != nil {
		infrastructure.ErrLog.Printf("Problem with Login Request - error getting JWT: %v,n", err)
		return nil, "", "", err
	}

	// err = s.userRepository.ChangePermissionWhenLogin(username)
	// if err != nil {
	// 	infrastructure.ErrLog.Printf(err.Error())
	// 	return user, tokenString, refreshToken, err
	// }
	return user, tokenString, refreshToken, nil

}

func (s *userService) LoginWithToken(token string) (*model.User, string, string, bool, error) {
	user, err := middleware.GetClaimsData(token)
	if err != nil {
		infrastructure.ErrLog.Println("Problem with LoginWithToken at GetClaimsData: ", err)
		return nil, "Invalid token", "", false, err
	}

	timeLeft := user.VerifyExpiresAt(time.Now().UnixNano()/infrastructure.NANO_TO_SECOND, true)
	if !timeLeft {
		infrastructure.ErrLog.Printf("Session expired!")
		return nil, "Token exceeded expire time!", "", false, nil
	}

	if ok, err := s.userRepository.LoginTokenRequest(user); err != nil {
		infrastructure.ErrLog.Printf("Problem with LoginWithToken: %v/n", err)
	} else {
		if !ok {
			return nil, "Invalid token", "", false, nil
		}
	}

	newTokenString, newRefreshTokenString, err := middleware.GetTokenString(user)
	if err != nil {
		infrastructure.ErrLog.Printf("Problem with LoginRequest at GetTokenString: %v/n", err)
		return nil, "", "", false, err
	}

	return user, newTokenString, newRefreshTokenString, true, nil
}

// func (s *userService) SetPermission(permission bool, receiverUsername string) (*model.UserResponse, error) {
// 	user, err := s.userRepository.SetPermission(permission, receiverUsername)
// 	if err != nil {
// 		infrastructure.ErrLog.Printf(err.Error())
// 		return nil, err
// 	}

// 	userResponse := &model.UserResponse{
// 		Id:           user.Id,
// 		Username:     user.Username,
// 		LocationName: user.LocationName,
// 		Role:         user.Role,
// 	}
// 	return userResponse, nil
// }

func (s *userService) GetByUsername(username string) (*model.UserResponse, error) {
	user, err := s.userRepository.GetByUsername(username)
	if err != nil {
		infrastructure.ErrLog.Printf(err.Error())
		return nil, err
	}

	userResponse := &model.UserResponse{
		Id:           user.Id,
		Role:         user.Role,
		Username:     user.Username,
		LocationName: user.LocationName,
		Permission:   user.Permission,
		StartTime:    user.StartTime,
		EndTime:      user.EndTime,
		Progress:     user.Progress,
	}
	return userResponse, nil
}

func (s *userService) SetPermission(permission bool, receiverUsername string, startTime *time.Time, endTime *time.Time) (*model.UserResponse, error) {
	usr, err := s.userRepository.SetPermission(permission, receiverUsername, startTime, endTime)
	if err != nil {
		infrastructure.ErrLog.Printf(err.Error())
		return nil, err
	}
	return &model.UserResponse{
		Id:           usr.Id,
		Username:     usr.Username,
		LocationName: usr.LocationName,
		Role:         usr.Role,
		Permission:   usr.Permission,
		StartTime:    usr.StartTime,
		EndTime:      usr.EndTime,
	}, nil
}

func (s *userService) GetChildUser(username string) ([]model.UserResponse, error) {
	childUsers, err := s.userRepository.GetChildUser(username)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	var childUsersResponse []model.UserResponse
	for i := range childUsers {
		child := model.UserResponse{
			Id:           childUsers[i].Id,
			Username:     childUsers[i].Username,
			Role:         childUsers[i].Role,
			LocationName: childUsers[i].LocationName,
			Permission:   childUsers[i].Permission,
			StartTime:    childUsers[i].StartTime,
			EndTime:      childUsers[i].EndTime,
			Progress:     childUsers[i].Progress,
		}
		childUsersResponse = append(childUsersResponse, child)
	}

	return childUsersResponse, nil
}

func (s *userService) GetChildCitizen(username string) ([]model.Citizen, error) {
	childCitizens, err := s.userRepository.GetChildCitizen(username)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return childCitizens, nil
}

func (s *userService) GetCensusProgress(username string) (interface{}, error) {
	return s.userRepository.GetCensusProgress(username)
}

func (s *userService) ChangePermissionOnly(username string) error {
	err := s.userRepository.ChangePermissionWhenLogin(username)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func (s *userService) SetProgress(username string, progress int) (*model.UserResponse, error) {
	user, err := s.userRepository.SetProgress(username, progress)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}
	userResponse := &model.UserResponse{
		Id:           user.Id,
		Username:     user.Username,
		LocationName: user.LocationName,
		Role:         user.Role,
		Permission:   user.Permission,
		StartTime:    user.StartTime,
		EndTime:      user.EndTime,
		Progress:     user.Progress,
	}

	return userResponse, nil
}
func (s *userService) FalsePermissionToChild(username string) error {
	return s.userRepository.FalsePermissionToChild(username)
}

func (s *userService) GetSexChart(username string) (*model.SexChartData, error) {
	data, err := s.userRepository.GetSexChart(username)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}
	return data, nil
}

func (s *userService) GetAgeChart(username string) (*model.AgeChartData, error) {
	ageData, err := s.userRepository.GetAgeChart(username)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return ageData, nil
}
func NewUserService() UserService {
	return &userService{
		userRepository: repository.NewUserRepository(),
	}
}

func checkPassword(user *model.User, password string) error {
	// check password: hashed one.
	// compare: hashed, plain
	if !comparePassword(user.Password, password) {
		return errors.New("incorrect password from service/checkPassword")
	}

	return nil
}

func (s *userService) CheckCredentials(id int, password string) (*model.User, error) {
	user, err := s.userRepository.GetById(id)
	if err != nil {
		return nil, err
	}
	if !comparePassword(user.Password, password) {
		return nil, errors.New("incorrect password from service/check credential")
	}
	return user, nil
}
func comparePassword(hashedPwd string, plainPwd string) bool {
	// cmphashandpass : true ~ nil.
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd)); err != nil {
		return false
	}
	return true
}

func hashAndSalt(password string) string {
	hashedPwd, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println(err.Error() + "/service/hashAndSalt")
	}
	// log.Println("hashedPwd is:", string(hashedPwd))
	return string(hashedPwd)
}
