package service

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/repository"
	"citizenv/utils"
	"log"
)

type citizenService struct {
	citizenRepository model.CitizenRepository
}

type CitizenService interface {
	GetAll() ([]model.Citizen, error)
	GetById(id int) (*model.Citizen, error)
	GetByIdentityNumber(identityNumber int) (*model.Citizen, error)
	CreateCitizen(newCitizen *model.CitizenPayload) (*model.Citizen, error)
	DeleteCitizen(id int) error
	UpdateCitizen(citizenInfo model.Citizen) (*model.Citizen, error)
}

func (s *citizenService) GetAll() ([]model.Citizen, error) {
	return s.citizenRepository.GetAll()
}

func (s *citizenService) GetById(id int) (*model.Citizen, error) {
	return s.citizenRepository.GetById(id)
}

func (s *citizenService) GetByIdentityNumber(identityNumber int) (*model.Citizen, error) {
	return s.citizenRepository.GetByIdentityNumber(identityNumber)
}

func (s *citizenService) CreateCitizen(newCitizen *model.CitizenPayload) (*model.Citizen, error) {
	// turn time string into time.Time
	cvtedTime, err := utils.StringToTimeConverter(newCitizen.Birthday)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	citizenModel := &model.Citizen{
		IdentityNumber:   newCitizen.IdentityNumber,
		Name:             newCitizen.Name,
		Birthday:         cvtedTime,
		Sex:              newCitizen.Sex,
		Religion:         newCitizen.Religion,
		EducationalLevel: newCitizen.EducationalLevel,
		Job:              newCitizen.Job,
		OriginAddress:    newCitizen.OriginAddress,
		PermanentAddress: newCitizen.PermanentAddress,
		TempAddress:      newCitizen.TempAddress,
		InputUsername:    newCitizen.InputUsername,
	}
	citizen, err := s.citizenRepository.CreateCitizen(citizenModel)
	if err != nil {
		infrastructure.ErrLog.Println(err.Error())
		return nil, err
	}

	return citizen, nil

}

func (s *citizenService) DeleteCitizen(id int) error {
	err := s.citizenRepository.DeleteCitizen(id)
	if err != nil {
		infrastructure.ErrLog.Println(err)
		return err
	}

	return nil
}

func (s *citizenService) UpdateCitizen(citizenInfo model.Citizen) (*model.Citizen, error) {
	citizen, err := s.citizenRepository.UpdateCitizen(citizenInfo)
	if err != nil {
		infrastructure.ErrLog.Println(err)
		return nil, err
	}

	return citizen, nil
}
func NewCitizenService() CitizenService {
	return &citizenService{
		citizenRepository: repository.NewCitizenRepository(),
	}
}
