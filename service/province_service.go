package service

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/repository"
)

type provinceService struct {
	provinceRepository model.ProvinceRepository
}

type ProvinceService interface {
	GetAll() ([]model.Province, error)
	GetById(id int) (*model.Province, error)
	GetByProvinceCode(code string) (*model.Province, error)
	GetByProvinceName(name string) (*model.Province, error)
	CreateProvince(newProvince *model.Province) (*model.Province, error)
	DeleteByProvinceId(id int) error
	DeleteByProvinceCode(code string) error
}

func (s *provinceService) GetAll() ([]model.Province, error) {
	return s.provinceRepository.GetAll()
}

func (s *provinceService) GetById(id int) (*model.Province, error) {
	province, err := s.provinceRepository.GetById(id)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return province, nil
}

func (s *provinceService) GetByProvinceCode(code string) (*model.Province, error) {
	province, err := s.provinceRepository.GetByProvinceCode(code)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return province, nil
}

func (s *provinceService) GetByProvinceName(name string) (*model.Province, error) {
	province, err := s.provinceRepository.GetByProvinceName(name)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return province, nil
}

func (s *provinceService) CreateProvince(newProvince *model.Province) (*model.Province, error) {
	newProvince, err := s.provinceRepository.CreateProvince(newProvince)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return newProvince, err
}

func (s *provinceService) DeleteByProvinceId(id int) error {
	if err := s.provinceRepository.DeleteByProvinceId(id); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func (s *provinceService) DeleteByProvinceCode(code string) error {
	if err := s.provinceRepository.DeleteByProvinceCode(code); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func NewProvinceService() ProvinceService {
	return &provinceService{
		provinceRepository: repository.NewProvinceRepository(),
	}
}
