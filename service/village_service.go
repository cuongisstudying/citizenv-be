package service

import (
	"citizenv/infrastructure"
	"citizenv/model"
	"citizenv/repository"
)

type villageService struct {
	villageRepository model.VillageRepository
}

type VillageService interface {
	GetAll() ([]model.Village, error)
	GetById(id int) (*model.Village, error)
	GetByVillageCode(code string) (*model.Village, error)
	GetByVillageName(name string) (*model.Village, error)
	CreateVillage(newVillage *model.Village) (*model.Village, error)
	DeleteByVillageId(id int) error
	DeleteByVillageCode(code string) error
}

func (s *villageService) GetAll() ([]model.Village, error) {
	return s.villageRepository.GetAll()
}

func (s *villageService) GetById(id int) (*model.Village, error) {
	village, err := s.villageRepository.GetById(id)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return village, nil
}

func (s *villageService) GetByVillageCode(code string) (*model.Village, error) {
	village, err := s.villageRepository.GetByVillageCode(code)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return village, nil
}

func (s *villageService) GetByVillageName(name string) (*model.Village, error) {
	village, err := s.villageRepository.GetByVillageName(name)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return village, nil
}

func (s *villageService) CreateVillage(newVillage *model.Village) (*model.Village, error) {
	newVillage, err := s.villageRepository.CreateVillage(newVillage)
	if err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return nil, err
	}

	return newVillage, err
}

func (s *villageService) DeleteByVillageId(id int) error {
	if err := s.villageRepository.DeleteByVillageId(id); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func (s *villageService) DeleteByVillageCode(code string) error {
	if err := s.villageRepository.DeleteByVillageCode(code); err != nil {
		infrastructure.ErrLog.Println("error: ", err.Error())
		return err
	}

	return nil
}

func NewVillageService() VillageService {
	return &villageService{
		villageRepository: repository.NewVillageRepository(),
	}
}
