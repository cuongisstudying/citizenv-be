package main

// @title Swagger citizenV
// @version 1.0
// @description CitizenV project APIs list

// @host localhost:10050
// @BasePath /api/v1

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
