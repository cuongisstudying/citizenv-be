# Start from the golang base image
FROM golang:1.16-alpine as builder

# Set maintainer
LABEL Maintainer="Cuong Dang Trung <cuongisstudying@gmail.com>"

# Set HEADER AND ENV FILES
ARG HEADER_FILE
ARG ENV_FILE
ENV HEADER_FILE=$HEADER_FILE
ENV ENV_FILE=$ENV_FILE

# Check HEADER_FILE & ENV_FILE
RUN echo "File swagger: $HEADER_FILE"
RUN echo "File env: $ENV_FILE"

RUN apk add bash ca-certificates git gcc g++ libc-dev

# Set WORKDIR
RUN mkdir -p /work/citizenv
WORKDIR /work/citizenv

# Copy go.mod and go.sum
COPY go.mod .
COPY go.sum .
RUN ls -la /work/citizenv/

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# COPY everything else
COPY . /work/citizenv/
COPY $ENV_FILE /work/citizenv/.env

RUN go get -u github.com/swaggo/swag/cmd/swag
RUN swag init --parseDependency -g $HEADER_FILE

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o citizenv .