package utils

import (
	"log"
	"strings"
	"time"
)

func StringToTimeConverter(tmString string) (*time.Time, error) {
	tm, err := time.Parse(time.RFC3339Nano, tmString)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return &tm, nil
}

func InTimeSpan(startTime, endTime, current time.Time) bool {
	if startTime.Before(endTime) && startTime.Before(current) && current.Before(endTime) {
		return true
	}

	return false
}

func NowTimeConverter() *time.Time {
	t := time.Now()
	splitTimeNow := strings.Split(t.String(), " ")
	nowTimeString := splitTimeNow[0] + "T" + splitTimeNow[1] + "Z"
	nowTime, _ := StringToTimeConverter(nowTimeString)
	return nowTime
}
