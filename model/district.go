package model

type District struct {
	Id           int    `json:"id" gorm:"unique;autoIncrement:true"`
	Name         string `json:"name" gorm:"column:name"`
	Code         string `json:"code" gorm:"primaryKey;autoIncrement:false;column:code"`
	ProvinceCode string `json:"province_code" gorm:"column:province_code"`
	TotalCitizen int    `json:"totalCitizen" gorm:"column:total_citizen"`
	TotalInput   int    `json:"totalInput" gorm:"column:total_input;default:0"`
	Wards        []Ward `json:"wards" gorm:"foreignKey:DistrictCode;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" swaggerignore:"true"`
}

type DistrictRepository interface {
	GetAll() ([]District, error)
	GetById(id int) (*District, error)
	GetByDistrictCode(code string) (*District, error)
	GetByDistrictName(name string) (*District, error)
	CreateDistrict(newDistrict *District) (*District, error)
	DeleteByDistrictId(id int) error
	DeleteByDistrictCode(code string) error
}
