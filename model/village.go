package model

type Village struct {
	Id           int    `json:"id" gorm:"unique;autoIncrement:true"`
	Name         string `json:"name" gorm:"column:name"`
	Code         string `json:"code" gorm:"primaryKey;autoIncrement:false;column:code"`
	TotalCitizen int    `json:"totalCitizen" gorm:"column:total_citizen"`
	TotalInput   int    `json:"totalInput" gorm:"column:total_input;default:0"`
	WardCode     string `json:"ward_code" gorm:"column:ward_code"`
	//Citizens     []Citizen `json:"citizens" gorm:"foreignKey:PermanentAddressCode;constraint:OnUpdate:CASCADE, OnDelete:CASCADE" swaggerignore:"true"`
}

type VillageRepository interface {
	GetAll() ([]Village, error)
	GetById(id int) (*Village, error)
	GetByVillageCode(code string) (*Village, error)
	GetByVillageName(name string) (*Village, error)
	CreateVillage(newVilalge *Village) (*Village, error)
	DeleteByVillageId(id int) error
	DeleteByVillageCode(code string) error
}
