package model

type Ward struct {
	Id           int       `json:"id" gorm:"unique;autoIncrement:true"`
	Name         string    `json:"name" gorm:"column:name"`
	Code         string    `json:"code" gorm:"primaryKey;autoIncrement:false;column:code"`
	TotalCitizen int       `json:"totalCitizen" gorm:"column:total_citizen"`
	DistrictCode string    `json:"district_code" gorm:"column:district_code"`
	TotalInput   int       `json:"totalInput" gorm:"column:total_input;default:0"`
	Villages     []Village `json:"villages" gorm:"foreignKey:WardCode;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" swaggerignore:"true"`
}

type WardRepository interface {
	GetAll() ([]Ward, error)
	GetById(id int) (*Ward, error)
	GetByWardCode(code string) (*Ward, error)
	GetByWardName(name string) (*Ward, error)
	CreateWard(newWard *Ward) (*Ward, error)
	DeleteByWardId(id int) error
	DeleteByWardCode(code string) error
}
