package model

type Province struct {
	Id           int        `json:"id" gorm:"unique;autoIncrement:true"`
	Name         string     `json:"name" gorm:"column:name"`
	Code         string     `json:"code" gorm:"primaryKey;autoIncrement:false;column:code"`
	TotalCitizen int        `json:"totalCitizen" gorm:"column:total_citizen"`
	TotalInput   int        `json:"totalInput" gorm:"column:total_input;default:0"`
	Districts    []District `json:"districts" gorm:"foreignKey:ProvinceCode;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" swaggerignore:"true"`
}

type ProvinceRepository interface {
	GetAll() ([]Province, error)
	GetById(id int) (*Province, error)
	GetByProvinceCode(code string) (*Province, error)
	GetByProvinceName(name string) (*Province, error)
	CreateProvince(newProvince *Province) (*Province, error)
	DeleteByProvinceId(id int) error
	DeleteByProvinceCode(code string) error
}
