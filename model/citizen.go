package model

import "time"

type Citizen struct {
	Id               int        `json:"id" gorm:"primaryKey"`
	IdentityNumber   int        `json:"identityNumber" gorm:"column:ident_number"`
	Name             string     `json:"name" gorm:"column:name"`
	Birthday         *time.Time `json:"birthday" gorm:"column:birthday"`
	Sex              string     `json:"sex" gorm:"column:sex"`
	Religion         string     `json:"religion" gorm:"column:religion"`
	EducationalLevel string     `json:"educationalLevel" gorm:"column:edu_level"`
	Job              string     `json:"job" gorm:"column:job"`
	OriginAddress    string     `json:"originAddress" gorm:"column:origin_address"`       // quê quán
	PermanentAddress string     `json:"permanentAddress" gorm:"column:permanent_address"` //  thường trú
	TempAddress      string     `json:"tempAddress" gorm:"column:temp_address"`           // tạm trú
	InputUsername    string     `json:"inputUsername" gorm:"column:input_username"`
	InputLocation    string     `json:"inputLocation" gorm:"column:input_location"`
}

type CitizenPayload struct {
	IdentityNumber   int    `json:"identityNumber"`
	Name             string `json:"name"`
	Birthday         string `json:"birthday"`
	Sex              string `json:"sex"`
	Religion         string `json:"religion"`
	EducationalLevel string `json:"educationalLevel"`
	Job              string `json:"job"`
	OriginAddress    string `json:"originAddress"`    // quê quán
	PermanentAddress string `json:"permanentAddress"` //  thường trú
	TempAddress      string `json:"tempAddress"`      // tạm trú
	InputUsername    string `json:"inputUsername"`
}
type CitizenRepository interface {
	GetAll() ([]Citizen, error)
	GetById(id int) (*Citizen, error)
	GetByIdentityNumber(identityNumber int) (*Citizen, error)
	CreateCitizen(newCitizens *Citizen) (*Citizen, error)
	DeleteCitizen(id int) error
	UpdateCitizen(newCitizen Citizen) (*Citizen, error)
}
